/*
 * AtomicDevs.cpp
 *
 *  Created on: Feb 25, 2015
 *      Author: paul
 */

#include "AtomicDevs.h"
#include "exceptions/RuntimeDevsException.h"
#include <algorithm>
#include <cmath>
#include <ctime>
#include <algorithm>
#include <limits>
#include <iostream>

namespace cdevs {

/**
 * \brief Constructor
 *
 * @param name  The name of the model
 */
AtomicDevs::AtomicDevs(std::string name)
	: BaseDevs(name), relocatable_(true)
{
	last_read_time_.set_x(0);
	last_read_time_.set_y(0);

}

/**
 * \brief Sets the location of the model
 *
 * @param location  The location to move the model onto
 * @param force  Flag determining whether the location be forced when negative
 */
void AtomicDevs::set_location(int location, bool force)
{
	if (location_ < 0 || force) {
		location_ = location;
	}
}

/**
 * \brief Sets the GVT for the model
 *
 * @param new_gvt  The new GVT value
 * @param last_state_only
 */
void AtomicDevs::set_GVT(double new_gvt, bool last_state_only)
{
	// if the states are empty, that means we don't have any memory of the past
	// so throw an exception
	if(old_states_.empty())
		throw RuntimeDevsException("Model has no memory of the past");

	// keep the last state
	DevsSnapshot last_state = old_states_.back();

	// find the first state with a timestamp greater than the GVT
	old_states_.erase(std::remove_if(old_states_.begin(), old_states_.end(),
		[&](const DevsSnapshot& state){ return state.time_last_.get_x() >= new_gvt; }), old_states_.end());

	// if the old states is empty now, re-push the last state
	if(old_states_.empty())
		old_states_.push_back(last_state);
}

/**
 * \brief Reverts atomic devs to specified time
 *
 * @param time  The time to revert to
 * @param memo
 */
bool AtomicDevs::Revert(DevsTime time)
{
	// If we don't have any old states
	if (this->old_states_.empty())
		throw RuntimeDevsException("Got empty old states for AtomicDevs at revert");

	auto iterator = std::find_if(old_states_.rbegin(), old_states_.rend(),
	        [time](const DevsSnapshot& state) {return state.time_last_ < time;});

	DevsSnapshot state;

	if (iterator == old_states_.rend())
		state = old_states_.front();
	else
		state = (*iterator);

	this->time_last_ = state.time_last_;
	this->time_next_ = state.time_next_;

	old_states_.erase(old_states_.begin(), old_states_.begin() + (old_states_.rend() - iterator));

	if (last_read_time_ > time) {
		last_read_time_.set_x(0);
		last_read_time_.set_y(0);
		return true;
	}

	return false;
}

/**
 * \brief The output function of the model
 *
 * @return  A map with the model ports as key and events as values
 */
std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const EventBase> >, std::owner_less<std::weak_ptr<Port> > > AtomicDevs::OutputFunction()
{
	return {};
}

/**
 * \brief The time advance function of the model
 *
 * @return  The time this model needs to advance
 */
double AtomicDevs::TimeAdvance()
{
	return std::numeric_limits<double>::infinity();
}

/**
 * \brief Performs the transitions up to the given time
 *
 * @param until_time The time up to which transitions should be performed
 * @return True if the there was a delayed transition performed
 */
bool AtomicDevs::PerformDelayedTransition(DevsTime until_time)
{
	if (!delayed_transitions_.empty()) {
		DevsTime transition = delayed_transitions_.top();

		if (transition < get_time_next() && transition < until_time) {
			setState(ExtTransition(my_input_));
			delayed_transitions_.pop();
			return true;
		}
	}

	return false;
}

/**
 * \brief Takes a snapshot of the atomic devs, storing all current information about its state
 */
void AtomicDevs::TakeSnapshot()
{
	DevsSnapshot devs_snapshot;
	devs_snapshot.time_last_ = get_time_last();
	devs_snapshot.time_next_ = get_time_next();
	devs_snapshot.input_ = my_input_;
	devs_snapshot.elapsed_ = elapsed_;

	old_states_.push_back(devs_snapshot);
}

/**
 * \brief Takes an initial snapshot of the atomic devs, storing all current information about its state
 */
void AtomicDevs::TakeInitialSnapshot()
{
	DevsSnapshot devs_snapshot;
	devs_snapshot.time_last_ = get_time_last();
	devs_snapshot.time_next_ = get_time_next();
	devs_snapshot.input_ = {};
	devs_snapshot.elapsed_ = 0.0;

	old_states_.push_back(devs_snapshot);
}


/**
 * \brief Destructor
 */
AtomicDevs::~AtomicDevs()
{
}

/**
 * \brief Flattens the connections
 */
void AtomicDevs::FlattenConnections()
{
	for (auto& port : input_ports_)
		port->set_host_devs(getPtr());
	for (auto& port : output_ports_)
		port->set_host_devs(getPtr());
}

/**
 * \brief Unflattens the connections
 */
void AtomicDevs::UnflattenConnections()
{
	for (auto& port : input_ports_)
		port->ResetHostDEVS();
	for (auto& port : output_ports_)
		port->ResetHostDEVS();
}

/**
 * \brief Finalize an atomic devs, preparing it for the simulation
 *
 * @param name  The name to precede the model's name
 * @param model_counter The counter so that each model has a unique ID
 * @param model_ids  An id to model mapping
 * @param select_hierarchy  The select hierarchy
 */
int AtomicDevs::Finalize(std::string name, int model_counter, std::map<int, std::shared_ptr<BaseDevs>> & model_ids,
        std::list<std::shared_ptr<BaseDevs>> select_hierarchy)
{
	// Give a name
	this->full_name_ = name + get_model_name();

	// Give a unique ID to the model itself
	model_id_ = model_counter;
	select_hierarchy_ = select_hierarchy; // optimizable by allocating enough space for this model as well
	select_hierarchy_.push_back(getPtr());

	// Add the element to its designated place in the model_ids list
	model_ids[model_id_] = getPtr();

	// Return the unique ID counter, incremented so it stays unique
	return ++model_counter;
}

/**
 * \brief Adds a delayed transition to the AtomicDevs
 *
 * @param time The time to perform the transition
 * @param state The state to transition to
 */
void AtomicDevs::AddDelayedTransition(DevsTime time)
{
	delayed_transitions_.push(time);
}

}

