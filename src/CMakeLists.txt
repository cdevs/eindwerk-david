#=====================================================================#
# Sources                                                             #
#=====================================================================#
# SOURCES contains all the source files in the project except for those
# with only google test purposes. Google tests should always be written
# in a separate file and added to the TESTSOURCES set. Separate Test runs
# should not be added in the TESTSOURCES set but be added as executable
# with their dependencies linked.

set(SOURCES 
	AtomicDevs.cpp 
	BaseDEVS.cpp 
	BaseSimulator.cpp 
	ClassicDEVSWrapper.cpp 
	Controller.cpp 
	CoupledDEVS.cpp 
	DevsTime.cpp 
	GvtControlMessage.cpp 
	Message.cpp
	MessageScheduler.cpp 
	RootDEVS.cpp  
	Simulator.cpp 
	Solver.cpp  
	ThreadEvent.cpp 
	Tracers.cpp 
	Port.cpp  
	allocators/AutoAllocator.cpp 
	exceptions/LogicDevsException.cpp
	exceptions/RuntimeDevsException.cpp
	schedulers/SchedulerAH.cpp  
	schedulers/SchedulerML.cpp 
	schedulers/SchedulerSL.cpp     
	
	tracers/Tracer.cpp
	tracers/TracerAction.cpp 
	tracers/TracerXML.cpp 
	tracers/TracerVerbose.cpp 
	tracers/TracerJson.cpp         
	)


set(TESTSOURCES  
	AtomicDevs_test.cpp 
	BaseDEVS_test.cpp 
	BaseSimulator_test.cpp 
	ClassicDEVSWrapper_test.cpp 
	CoupledDEVS_test.cpp 
	GvtControlMessage_test.cpp 
	RootDEVS_test.cpp  
	Simulator_test.cpp 
	Solver_test.cpp 
	ThreadEvent_test.cpp 
	Message_test.cpp
	Tracers_test.cpp 
	allocators/AutoAllocator_test.cpp
	tracers/TracerVerbose_test.cpp
	tracers/TracerXML_test.cpp
	tracers/TracerJson_test.cpp
	schedulers/SchedulerML_test.cpp
	schedulers/SchedulerAH_test.cpp
	schedulers/SchedulerSL_test.cpp
	utility_test.cpp 
	scenario_test.cpp
	)
	
# Pull in the examples includes
include_directories(${PROJECT_SOURCE_DIR}/examples)

#=====================================================================#
# Executables                                                         #
#=====================================================================#
# All executables and their linkage should be done here

set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

add_library(cdevs SHARED ${SOURCES})

add_executable (test_all test_all.cpp ${TESTSOURCES} ${EXAMPLESOURCES})
target_link_libraries(test_all cdevs gtest gtest_main pthread dl)
