#include "MessageScheduler.h"

namespace cdevs {
/**
 * \brief Constructor
 */
MessageScheduler::MessageScheduler()
{
}

/**
 * \brief Destructor
 */
MessageScheduler::~MessageScheduler()
{

}

/**
 * \brief Insert several messages that were created elsewhere and merge them in.
 *
 * @param extraction The output of the extract method on the other message scheduler
 * @param model_list Models that are inserted and for which extraction happened
 */
void MessageScheduler::Insert(std::vector<Message> messages, std::vector<Model> models)
{
	throw RuntimeDevsException("MessageScheduler::Insert not implemented yet!");
}

/**
 * \brief Extract messages from the message scheduler for when a model gets removed from this kernel.
 *
 * @param model_ids Iterable of model_ids of models that will be removed from this node
 * @return Extraction that needs to be passed to the insert method of another scheduler
 * */
void MessageScheduler::Extract(std::vector<int> modelIds)
{
	throw RuntimeDevsException("MessageScheduler::Extract not implemented yet!");
}

/**
 * \brief Schedule a message for processing
 *
 * @param msg The message to schedule
 */
void MessageScheduler::Schedule(Message message)
{
	// remove it from invalidated messages
	auto it = message_invalids_.find(message);
	if (it != message_invalids_.end())
		message_invalids_.erase(it);

	// push it to the queue
	message_queue_.push(message);
}

/**
 * \brief Unschedule several messages, this way it will no longer be processed.
 *
 * @param uuids Iterable of UUIDs that need to be removed
 */
void MessageScheduler::MassUnschedule(std::list<Message> messages)
{
	for (Message& msg : messages) {
		message_invalids_.insert(msg);
	}
}

/**
 * \brief Returns the first (valid) message. Not necessarily O(1), as it could be
 * the case that a lot of invalid messages are still to be deleted.
 *
 * @return First message
 */
Message MessageScheduler::ReadFirst()
{
	CleanFirst();

	Message msg = message_queue_.top();
	return msg;
}

/**
 * \brief Notify that the first (valid) message is processed.
 *
 * @return The next first message that is valid
 */
void MessageScheduler::RemoveFirst()
{
	CleanFirst();

	if (message_queue_.empty())
		return;

	Message message = message_queue_.top();
	message_queue_.pop();

	message_processed_.push_back(message);
}

/**
 * Notify that the first (valid) message must be removed
 *
 * @return The next first message that is valid
 */
void MessageScheduler::PurgeFirst()
{
	CleanFirst();
	message_queue_.pop();
}

/**
 * \brief Clean all invalid messages at the front of the list.
 * Method MUST be called before any accesses should happen to the first element,
 * otherwise this first element might be a message that was just invalidated
 */
void MessageScheduler::CleanFirst()
{
	if (message_queue_.empty())
		return;

	// get the top message
	Message message = message_queue_.top();

	// check if it is invalidated
	auto it = message_invalids_.find(message);

	// if it is in our invalids container, that means we can simply pop it off
	if (it != message_invalids_.end()) {
		// delete *it;
		message_invalids_.erase(it);
		message_queue_.pop();
	}
}

/**
 * \brief Clean up the processed list, also removes all invalid elements
 *
 * @param time Time up to which cleanups are allowed to happen
 */
void MessageScheduler::CleanUp(DevsTime time)
{
	// keep a vector, which will only keep message whose timestamp is greater than the given time
	std::vector<Message> message_cleaned;

	for(auto& message : message_processed_)
	{
		// we found a message who is newer than the given time, so push it
		if(message.get_timestamp() >= time)
			message_cleaned.push_back(message);
	}

	// replace processed map with clean one
	message_processed_ = message_cleaned;
}

/**
 * \brief Revert the inputqueue to the specified time, will also clean up the list of processed elements
 *
 * @param time  Time to which revertion should happen
 */
void MessageScheduler::Revert(DevsTime targetTime)
{
	std::vector<Message> incoming_messages;

	for (Message& message : message_processed_) {
		if (message.get_timestamp() < targetTime)
			message_queue_.push(message);
		else
			incoming_messages.push_back(message);
	}

	message_processed_ = incoming_messages;

}

/**
 * \brief Checks if the message scheduler is empty
 *
 * @return  True if the message scheduler is empty, false otherwise
 */
bool MessageScheduler::isEmpty() const
{
	return message_queue_.empty();
}
}
