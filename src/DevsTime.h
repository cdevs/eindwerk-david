#ifndef SRC_DEVSTIME_H_
#define SRC_DEVSTIME_H_

#include <string>

#include <cereal/access.hpp>

namespace cdevs {
/**
 * \brief Our notation of time
 *
 * The time object consist of two fields:
 * 	x - the time
 * 	y - age (= number of loops in DEVS simulation)
 */
class DevsTime
{
public:
	DevsTime();
	DevsTime(double x, double y);
	double get_x() const;
	void set_x(double x);
	int get_y() const;
	void set_y(int y);

	bool operator>(const DevsTime& g) const;
	bool operator>=(const DevsTime& g) const;
	bool operator<(const DevsTime& g) const;
	bool operator<=(const DevsTime& g) const;
	void operator=(const DevsTime& d);
	bool operator==(const DevsTime& g) const;

	std::string string() const;

private:
	friend class cereal::access;
	double x_;
	int y_;
	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(x_, y_);
	}
};
}
#endif /* SRC_DEVSTIME_H_ */
