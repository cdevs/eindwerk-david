/*
 * Port.h
 *
 *  Created on: 12-mrt.-2015
 *      Author: Ian
 */

#ifndef PORT_H_
#define PORT_H_

#include "utility.h"
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <cereal/access.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/map.hpp>
#include <cereal/types/vector.hpp>

namespace cdevs {

// Forward class declaration
class BaseDevs;

class Port
{
	friend class CoupledDevs;
	friend class Simulator;
	/**
	 * Class for DEVS model ports (both input and output). This class provides basic port attributes and query methods.
	 */
	friend class Solver;
public:
	Port(bool is_input = false, std::string = "");
	const std::string get_port_name() const;
	const std::string get_full_port_name() const;
	inline bool isInput() const;
	void set_host_devs(std::weak_ptr<BaseDevs> model);
	void ResetHostDEVS();
	inline std::weak_ptr<BaseDevs> GetHostDEVS() const;
	~Port();
	bool operator<(const Port& b) const;
	bool operator==(const Port& b) const;
	unsigned get_msg_count() const;
	void set_msg_count(unsigned msg_count);
	const std::weak_ptr<BaseDevs>& get_host_devs() const;
	const std::vector<std::weak_ptr<Port> >& get_inline() const;
	void set_inline(const std::vector<std::weak_ptr<Port> >& _inline);
	bool isis_inport() const;
	void set_is_inport(bool isInport);
	const std::string& get_name() const;
	void set_name(const std::string& name);
	const std::vector<std::weak_ptr<Port> >& get_outline() const;
	void set_outline(const std::vector<std::weak_ptr<Port> >& outline);
	const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& get_routing_inline() const;
	void set_routing_inline(
	        const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& routingInline);
	const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& get_routing_outline() const;
	void set_routing_outline(
	        const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& routingOutline);
	const std::shared_ptr<BaseDevs>& get_temp() const;
	void set_temp(const std::shared_ptr<BaseDevs>& temp);
	const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& get_functions() const;
	void set_functions(
	        const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& functions);

protected:
	//TODO: make private and add helper methods
	std::vector<std::weak_ptr<Port>> inline_;
		std::vector<std::weak_ptr<Port>> outline_;

		std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > > routing_inline_;
		std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > > routing_outline_;
		std::weak_ptr<BaseDevs> host_DEVS_;
		std::shared_ptr<BaseDevs> temp_;
		unsigned msg_count_;

		std::string name_;
		bool is_inport_;
		std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > > z_functions_;

private:
	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(inline_, outline_/*, routing_inline_, routing_outline_*/, host_DEVS_, msg_count_, name_, is_inport_/*,
		 z_functions*/);
	}
};

/**
 * \brief Gets the port is an input port
 *
 * @return  True if the port is an input port, false if it isn't
 */
inline bool Port::isInput() const
{
	return is_inport_;
}

/**
 * \brief Gets the port's host model
 *
 * @return  The host model of the port
 */
inline std::weak_ptr<BaseDevs> Port::GetHostDEVS() const
{
	return host_DEVS_;
}

inline const std::weak_ptr<BaseDevs>& Port::get_host_devs() const
{
	return host_DEVS_;
}

inline const std::vector<std::weak_ptr<Port> >& Port::get_inline() const
{
	return inline_;
}

inline void Port::set_inline(const std::vector<std::weak_ptr<Port> >& _inline)
{
	inline_ = _inline;
}

inline bool Port::isis_inport() const
{
	return is_inport_;
}

inline void Port::set_is_inport(bool isInport)
{
	is_inport_ = isInport;
}

inline const std::string& Port::get_name() const
{
	return name_;
}

inline void Port::set_name(const std::string& name)
{
	name_ = name;
}

inline const std::vector<std::weak_ptr<Port> >& Port::get_outline() const
{
	return outline_;
}

inline void Port::set_outline(const std::vector<std::weak_ptr<Port> >& outline)
{
	outline_ = outline;
}

inline const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& Port::get_routing_inline() const
{
	return routing_inline_;
}

inline void Port::set_routing_inline(
        const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& routingInline)
{
	routing_inline_ = routingInline;
}

inline const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& Port::get_routing_outline() const
{
	return routing_outline_;
}

inline void Port::set_routing_outline(
        const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& routingOutline)
{
	routing_outline_ = routingOutline;
}

inline const std::shared_ptr<BaseDevs>& Port::get_temp() const
{
	return temp_;
}

inline void Port::set_temp(const std::shared_ptr<BaseDevs>& temp)
{
	temp_ = temp;
}

inline const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& Port::get_functions() const
{
	return z_functions_;
}

inline void Port::set_functions(
        const std::map<std::weak_ptr<Port>, std::function<Event(Event)>, std::owner_less<std::weak_ptr<Port> > >& functions)
{
	z_functions_ = functions;
}

} /* namespace ns_DEVS */

#endif /* PORT_H_ */
