/*
 * TracerAction.cpp
 *
 *  Created on: 21 May 2015
 *      Author: ianvermeulen
 */

#include "TracerAction.h"

namespace cdevs {

/**
 * \brief Constructor
 */
TracerAction::TracerAction()
	: model_(), tracer_()
{

}

/**
 * \brief Constructor
 *
 * @param time  The time to perform the action
 * @param model  The model that is used in the action
 * @param text  The text that will be send to the tracer
 * @param tracer  The tracer to be used
 */
TracerAction::TracerAction(DevsTime time, std::shared_ptr<AtomicDevs> model, std::string text, std::weak_ptr<Tracer> tracer)
	: time_(time), model_(model), text_(text), tracer_(tracer)
{

}

/**
 * \brief Destructor
 */
TracerAction::~TracerAction()
{
}

/**
 * \brief Gets the text of the actions
 *
 * @return  Text of the action
 */
const std::string& TracerAction::getText() const
{
	return text_;
}

/**
 * \brief Sets the text of the actions
 *
 * @param  Text of the action
 */
void TracerAction::setText(const std::string& text)
{
	text_ = text;
}

/**
 * \brief Gets the tracer that performs the actions
 *
 * @return  Tracer of the action
 */
std::weak_ptr<Tracer> TracerAction::getTracer() const
{
	return tracer_;
}

/**
 * \brief Sets the tracer that performs the actions
 *
 * @param  Tracer of the action
 */
void TracerAction::setTracer(std::weak_ptr<Tracer> tracer)
{
	tracer_ = tracer;
}

/**
 * \brief Gets the model that is used in the action
 *
 * @return  Model of the action
 */
std::shared_ptr<AtomicDevs> TracerAction::getModel() const
{
	return model_.lock();
}

/**
 * \brief Sets the model that is used in the action
 *
 * @param model model of the action
 */
void TracerAction::setModel(std::shared_ptr<AtomicDevs> model)
{
	model_ = model;
}

/**
 * \brief Gets the time to perform the action
 *
 * @return  Time to perform the action
 */
const DevsTime& TracerAction::getTime() const
{
	return time_;
}

/**
 * \brief Sets the time to perform the action
 *
 * @param time Time to perform the action
 */
void TracerAction::setTime(const DevsTime& time)
{
	time_ = time;
}

} /* namespace cdevs */

