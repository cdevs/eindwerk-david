set(MODELNAME "PHOLDTest")

#=====================================================================#
# Sources                                                             #
#=====================================================================#
set(BENCHMARKSOURCES
   ${BENCHMARKSOURCES}
   ${CMAKE_CURRENT_SOURCE_DIR}/PHOLD.cpp
   CACHE INTERNAL "benchmark sources"
)

#=====================================================================#
# Executables                                                         #
#=====================================================================#
# All executables and their linkage should be done here

add_library(PHOLD SHARED PHOLD.cpp)
target_link_libraries(PHOLD cdevs)

add_executable (PHOLD_test experiment.cpp PHOLD.cpp)
target_link_libraries(PHOLD_test cdevs dl)

add_executable (PHOLD_resume resume.cpp PHOLD.cpp)
target_link_libraries(PHOLD_resume cdevs dl)