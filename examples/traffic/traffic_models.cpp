/*
 * traffic_models.cpp
 *
 *  Created on: 5-jun.-2015
 *      Author: david
 */

#include "traffic_models.h"
#include <cmath>
#include <memory>
#include <sstream>

using std::endl;
using std::make_shared;
using std::stringstream;
using std::to_string;

using cdevs::Atomic;

namespace cdevs_examples {
namespace traffic {
Car::Car(unsigned ID, float v, int v_pref, int dv_pos_max, int dv_neg_max, float departure_time)
	: ID_(ID), v_(v), v_pref_(v_pref), dv_pos_max_(dv_pos_max), dv_neg_max_(dv_neg_max), departure_time_(
	        departure_time), distance_travelled_(0), remaining_x_(0)
{
}
std::string Car::string() const
{
	stringstream ss;
	ss << "Car: ID = " << ID_ << ", v_pref = " << v_pref_ << ", dv_pos_max = " << dv_pos_max_ << ", dv_neg_max = "
	        << dv_neg_max_ << ", departure_time = " << departure_time_ << ", distance_travelled = "
	        << distance_travelled_ << ", v = " << v_ << ", path = " << path_ << endl;
	return ss.str();
}

bool Car::operator==(Car const& rhs) const
{
	return (ID_ == rhs.ID_) && (v_pref_ == rhs.v_pref_) && (dv_pos_max_ == rhs.dv_pos_max_)
	        && (dv_neg_max_ == rhs.dv_neg_max_) && (departure_time_ == rhs.departure_time_)
	        && (distance_travelled_ == rhs.distance_travelled_)
	        && (remaining_x_ == rhs.remaining_x_ && v_ == rhs.v_);
}

Query::Query(unsigned ID)
	: ID_(ID), direction_('\0')
{
}

std::string Query::string() const
{
	return "Query: ID = " + to_string(ID_);
}

bool Query::operator==(Query const& rhs) const
{
	return (ID_ == rhs.ID_) && (direction_ == rhs.direction_);
}

QueryAck::QueryAck(unsigned ID, float t_until_dep)
	: ID_(ID), t_until_dep_(t_until_dep)
{
}

std::string QueryAck::string() const
{
	return "Query Ack: ID = " + to_string(ID_) + ", t_until_dep = " + to_string(t_until_dep_);
}

bool QueryAck::operator==(QueryAck const& rhs) const
{
	return (ID_ == rhs.ID_) && (t_until_dep_ == rhs.t_until_dep_);
}

BuildingState::BuildingState(int IAT_min, int IAT_max, std::string path, std::string name)
	: IAT_min_(IAT_min), IAT_max_(IAT_max), path_(path), name_(name), current_time_(0), next_v_pref_(0), sent_(0)
{
	send_query_delay_ = path.empty() ? std::numeric_limits<double>::infinity() : Uniform(IAT_min, IAT_max);
	send_car_delay_ = std::numeric_limits<double>::infinity();
	int first_pos = name.find("_");
	int second_pos = name.find("_", first_pos + 1);
	send_query_id_ = atoi(name.substr(first_pos + 1, second_pos - 1).c_str()) * 1000
	        + atoi(name.substr(second_pos + 1, name.length()).c_str());
	send_car_id_ = send_query_id_;
}

std::string BuildingState::string() const
{
	if (!path_.empty()) {
		stringstream ss;
		ss << "Residence: send_query_delay = " << send_query_delay_ << ", send_query_id = " << send_query_id_
		        << ", send_car_delay = " << send_car_delay_ << ", send_car_id = " << send_car_id_ << endl;
		return ss.str();
	}

	return "Commercial: waiting...";
}

float BuildingState::Uniform(int min, int max)
{
	std::uniform_real_distribution<double> distribution(min, max);
	return distribution(generator_);
}

Building::Building(unsigned district, std::string path, std::string name, int IAT_min, int IAT_max, int v_pref_min,
        int v_pref_max, int dv_pos_max, int dv_neg_max)
	: Atomic(name), district_(district), path_(path), name_(name), IAT_min_(IAT_min), IAT_max_(IAT_max), v_pref_min_(
	        v_pref_min), v_pref_max_(v_pref_max), dv_pos_max_(dv_pos_max), dv_neg_max_(dv_neg_max), send_max_(1)
{
	// create output ports
	q_sans_ = AddOutPort("q_sans");
	q_send_ = AddOutPort("q_send");
	exit_ = AddOutPort("exit");

	// create input ports
	q_rans_ = AddInPort("q_rans");
	entry_ = AddInPort("entry");

	// set state
	setState(BuildingState(IAT_min, IAT_max, path, name));
	state_->next_v_pref_ = state_->Uniform(v_pref_min_, v_pref_max_);
}

BuildingState const& Building::IntTransition()
{
	double mintime = TimeAdvance();
	state_->current_time_ += mintime;

	state_->send_query_delay_ -= mintime;
	state_->send_car_delay_ -= mintime;

	if (state_->send_car_delay_ == 0) {
		state_->send_car_delay_ = std::numeric_limits<double>::infinity();
		state_->send_car_id_ = state_->send_query_id_;
		state_->next_v_pref_ = state_->Uniform(v_pref_min_, v_pref_max_);
	} else
		state_->send_query_delay_ = std::numeric_limits<double>::infinity();

	return *state_;
}

Outbags Building::OutputFunction()
{
	double mintime = TimeAdvance();
	float currentTime = state_->current_time_ + mintime;
	Outbags outputs;

	if (state_->send_car_delay_ == mintime) {
		outputs[exit_] = {make_shared<Car>(state_->send_car_id_, 0, state_->next_v_pref_, dv_pos_max_, dv_neg_max_, currentTime)};
	} else if (state_->send_query_delay_ == mintime) {
		outputs[q_send_] = {make_shared<Query>(state_->send_query_id_)};
	}

	return outputs;
}

double Building::TimeAdvance()
{
	return std::fmin(state_->send_query_delay_, state_->send_car_delay_);
}

BuildingState const& Building::ExtTransition(Outbags inputs)
{
	state_->current_time_ += elapsed_;
	state_->send_query_delay_ -= elapsed_;
	state_->send_car_delay_ -= elapsed_;
	Outbag const& queryAcks = (inputs.find(q_rans_) != inputs.end()) ? inputs.at(q_rans_) : Outbag();
	for (auto& qA : queryAcks) {
		if (qA->getType() != kQueryAck)
			continue;
		QueryAck const& queryAck = dynamic_cast<QueryAck const&>(*qA);
		if ((state_->send_car_id_ == queryAck.ID_) && (state_->sent_ < send_max_)) {
			state_->send_car_delay_ = queryAck.t_until_dep_;
			if (queryAck.t_until_dep_ < 20000) {
				state_->sent_ += 1;
				if (state_->sent_ < send_max_) {
					state_->send_query_delay_ = state_->Uniform(IAT_min_, IAT_max_);
					state_->send_query_id_ += 1000000;
				}
			}
		}
	}

	return *state_;
}

//Residence::Residence(std::string path, unsigned district, std::string name, int IAT_min, int IAT_max, int v_pref_min,
//        int v_pref_max, int dv_pos_max, int dv_neg_max)
//	: Building<Residence>(district, path, name)
//{
//}
//
//CommercialState::CommercialState()
//{
//}
//
//CommercialState::CommercialState(Car car)
//{
//	car_.reset(new Car(car));
//}
//
//std::string CommercialState::string() const
//{
//	return "CommercialState";
//}
//
//Commercial::Commercial(unsigned district, std::string name)
//	: Building<Commercial, CommercialState>(district, name)
//{
//	setState (CommercialState());to_collector_ = AddOutPort("toCollector");
//}
//
//CommercialState const& Commercial::ExtTransition(Outbags inputs)
//{
//	return CommercialState(dynamic_cast<Car>(*inputs.at(entry_).front()));
//}
//
//CommercialState const& Commercial::IntTransition()
//{
//	return CommercialState();
//}
//
//Outbags Commercial::OutputFunction()
//{
//	return { {	to_collector_, {state_->car_}}};
//}
//
//double Commercial::TimeAdvance()
//{
//	if (state_->car_.get() == nullptr)
//		return std::numeric_limits<double>::infinity();
//
//	return 0;
//}

RoadSegmentState::RoadSegmentState()
	: send_query_delay_(std::numeric_limits<double>::infinity()), send_query_id_(-1), send_ack_delay_(
	        std::numeric_limits<double>::infinity()), send_ack_id_(-1), send_car_delay_(
	        std::numeric_limits<double>::infinity()), send_car_id_(-1), last_car_(-1)
{
}

std::string RoadSegmentState::string() const
{
	return "";
}

bool RoadSegmentState::operator==(RoadSegmentState const& rhs) const
{

}

RoadSegment::RoadSegment(unsigned district, std::string name, double l, double v_max, double observ_delay)
	: Atomic(name), district_(district), l_(l), v_max_(v_max), observ_delay_(observ_delay), name_(name)
{
	// input ports
	q_rans_ = AddInPort("q_rans");
	q_recv_ = AddInPort("q_recv");
	car_in_ = AddInPort("car_in");
	entries_ = AddInPort("entries");
	q_rans_bs_ = AddInPort("q_rans_bs");
	q_recv_bs_ = AddInPort("q_recv_bs");

	// output ports
	q_sans_ = AddOutPort("q_sans");
	q_send_ = AddOutPort("q_send");
	car_out_ = AddOutPort("car_out");
	exits_ = AddOutPort("exits");

	q_sans_bs_ = AddOutPort("q_sans_bs");
}

RoadSegmentState const& RoadSegment::ExtTransition(Outbags inputs)
{

}

RoadSegmentState const& RoadSegment::IntTransition()
{

}

Outbags RoadSegment::OutputFunction()
{

}

double RoadSegment::TimeAdvance()
{
	return std::fmax(MinTime(), 0);
}

double RoadSegment::MinTime()
{
	return std::fmin(std::fmin(state_->send_query_delay_, state_->send_ack_delay_), state_->send_car_delay_);
}

Road::Road(unsigned district, std::string name, unsigned segments)
	: Coupled(name), district_(district), segments_(segments), segment_(segments)
{
	for (unsigned i = 0; i < segments_; i++) {
		std::string segment_name = name + "_" + to_string(i);
		segment_[i] = std::make_shared<RoadSegment>(district, segment_name);
		AddSubModel(segment_[i]);
	}

	q_rans_ = AddInPort("q_rans");
	q_recv_ = AddInPort("q_recv");
	car_in_ = AddInPort("car_in");
	entries_ = AddInPort("entries");
	q_rans_bs_ = AddInPort("q_rans_bs");
	q_recv_bs_ = AddInPort("q_recv_bs");

	// output ports
	q_sans_ = AddOutPort("q_sans");
	q_send_ = AddOutPort("q_send");
	car_out_ = AddOutPort("car_out");
	exits_ = AddOutPort("exits");
	q_sans_bs_ = AddOutPort("q_sans_bs");

	ConnectPorts(q_rans_, segment_.at(segments_ - 1)->q_rans_);
	ConnectPorts(q_recv_, segment_.at(0)->q_recv_);
	ConnectPorts(car_in_, segment_.at(0)->car_in_);
	ConnectPorts(entries_, segment_.at(0)->entries_);
	ConnectPorts(q_rans_bs_, segment_.at(0)->q_rans_bs_);
	ConnectPorts(q_recv_bs_, segment_.at(0)->q_recv_bs_);

	ConnectPorts(segment_.at(9)->q_sans_, q_sans_);
	ConnectPorts(segment_.at(segments_ - 1)->q_send_, q_send_);
	ConnectPorts(segment_.at(segments_ - 1)->car_out_, car_out_);
	ConnectPorts(segment_.at(0)->exits_, exits_);
	ConnectPorts(segment_.at(0)->q_sans_bs_, q_sans_bs_);

	for (unsigned i = 1; i < segments_; i++) {
		ConnectPorts(segment_[i]->q_sans_, segment_[i - 1]->q_rans_);
		ConnectPorts(segment_[i - 1]->q_send_, segment_[i]->q_recv_);
		ConnectPorts(segment_[i - 1]->car_out_, segment_[i]->car_in_);
	}
}

IntersectionState::IntersectionState(double switch_signal)
	: switch_signal_(switch_signal)
{
}

std::string IntersectionState::string() const
{
	return "";
}

Intersection::Intersection(unsigned district, std::string name, double switch_signal)
	: Atomic(name), district_(district), switch_signal_(switch_signal), switch_signal_delay_(switch_signal)
{
	state_.reset(new IntersectionState(switch_signal));

	q_send_ = {
		AddOutPort("q_send_to_n"),
		AddOutPort("q_send_to_e"),
		AddOutPort("q_send_to_s"),
		AddOutPort("q_send_to_w")
	};

	q_rans_ = {
		AddInPort("q_rans_to_n"),
		AddInPort("q_rans_to_e"),
		AddInPort("q_rans_to_s"),
		AddInPort("q_rans_to_w")
	};

	q_recv_ = {
		AddInPort("q_recv_to_n"),
		AddInPort("q_recv_to_e"),
		AddInPort("q_recv_to_s"),
		AddInPort("q_recv_to_w")
	};

	q_sans_ = {
		AddOutPort("q_sans_to_n"),
		AddOutPort("q_sans_to_e"),
		AddOutPort("q_sans_to_s"),
		AddOutPort("q_sans_to_w")
	};

	car_in_ = {
		AddInPort("car_in_to_n"),
		AddInPort("car_in_to_e"),
		AddInPort("car_in_to_s"),
		AddInPort("car_in_to_w")
	};

	car_out_ = {
		AddOutPort("car_out_to_n"),
		AddOutPort("car_out_to_e"),
		AddOutPort("car_out_to_s"),
		AddOutPort("car_out_to_w")
	};
}

IntersectionState const& Intersection::ExtTransition(Outbags inputs)
{
}

IntersectionState const& Intersection::IntTransition()
{
}

Outbags Intersection::OutputFunction()
{
}

double Intersection::TimeAdvance()
{
}

CollectorState::CollectorState()
{

}

std::string CollectorState::string() const
{

}

Collector::Collector() : Atomic("Collector"), district_(0)
{
	car_in_ = AddInPort("car_in");
}

CollectorState const& Collector::ExtTransition(Outbags inputs)
{
	for (auto& car : inputs.at(car_in_))
		state_->cars_.push_back(dynamic_cast<Car const&>(*car));

	return *state_;
}

}
}
