/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "model.h"
#include "Simulator.h"

#include <iostream>

#include <cereal/archives/binary.hpp>

using namespace cdevs;
using namespace cdevs_examples::galton;
using namespace std;

int main(int argc, char * argv[]) {
        //Arguments are Height, Balls per TA, Prob, Name
        std::shared_ptr<GaltonBoard> galton = GaltonBoard::create(10, 10, 0.50, "GaltonBoard");
        std::shared_ptr<GaltonBoard> galton2 = GaltonBoard::create(30, 100, 0.50, "GaltonBoard2");
        std::shared_ptr<GaltonBoard> galton3 = GaltonBoard::create(20, 100, 0.66, "GaltonBoardSkew");

        Simulator sim(galton);
        Simulator sim2(galton2);
        Simulator sim3(galton3);

        sim.set_termination_time(10.0);
        sim2.set_termination_time(100.0);
        sim3.set_termination_time(100.0);

        sim.set_checkpoint_interval(1);
        sim2.set_checkpoint_interval(0);
        sim3.set_checkpoint_interval(1);
        sim.set_checkpoint_name("galton1");
        sim2.set_checkpoint_name("galton2");
        sim3.set_checkpoint_name("galton3");

        sim3.set_gvt_interval(1);

        sim.set_classic_devs();
        sim3.set_classic_devs();

        sim.set_verbose("galton.txt");
        sim.set_json("galton.json");
        sim.set_xml("galton.xml");
        sim2.set_verbose("galton2.txt");
        sim3.set_verbose("galtonSkew.txt");

        sim.Simulate();

        std::string dist_csv = galton->getDistributionAsCSV();
        std::ofstream dist_stream("galton.csv");
        dist_stream << dist_csv << std::endl;
        dist_stream.close();

        std::string norm_dist_csv = galton->getNormalizedDistributionAsCSV();
        std::ofstream dist_norm_stream("galton_normalized.csv");
        dist_norm_stream << norm_dist_csv << std::endl;
        dist_norm_stream.close();

        sim2.Simulate();

        std::string dist_csv2 = galton2->getDistributionAsCSV();
        std::ofstream dist_stream2("galton2.csv");
        dist_stream2 << dist_csv2 << std::endl;
        dist_stream2.close();

        std::string norm_dist_csv2 = galton2->getNormalizedDistributionAsCSV();
        std::ofstream dist_norm_stream2("galton2_normalized.csv");
        dist_norm_stream2 << norm_dist_csv2 << std::endl;
        dist_norm_stream2.close();

        sim3.Simulate();

        std::string dist_csv3 = galton3->getDistributionAsCSV();
        std::ofstream dist_stream3("galtonSkew.csv");
        dist_stream3 << dist_csv3 << std::endl;
        dist_stream3.close();

        std::string norm_dist_csv3 = galton3->getNormalizedDistributionAsCSV();
        std::ofstream dist_norm_stream3("galtonSkew_normalized.csv");
        dist_norm_stream3 << norm_dist_csv3 << std::endl;
        dist_norm_stream3.close();

	return 0;
}
