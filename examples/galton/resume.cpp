/*
 * experiment.cpp
 *
 *  Created on: 5-mei-2015
 *      Author: david
 */

#include "model.h"
#include "Simulator.h"

using namespace cdevs;
using namespace cdevs_examples::galton;

int main(int argc, char * argv[])
{
	auto sim = Simulator::LoadFromCheckpoint("galton", 100);

	return 0;
}
