/*
 * AtomicModelTest.cpp
 *
 *  Created on: 31-mrt.-2015
 *      Author: david
 */
#include "model.h"
#include <chrono>
#include <cmath>
#include <iomanip>
#include <limits>
#include <sstream>

using namespace cdevs;
using std::to_string;

namespace cdevs_examples
{
namespace galton
{

Ball::Ball(unsigned id) : id_(id)
{
}

std::string Ball::string() const
{
	return "Ball " + to_string(id_);
}

std::string Ball::toXML() const
{
	return "<ball>" + to_string(id_) + "</ball>";
}

BallGeneratorState::BallGeneratorState() : count_(0)
{
}

BallGenerator::BallGenerator(double rate) : Atomic("BallGenerator"), rate_(rate)
{
	setState(BallGeneratorState());
	out_ = AddOutPort("OUT");
}

Outbags BallGenerator::OutputFunction()
{
	Outbags balls = {{out_, {}}};
	for (unsigned i = 0; i < rate_; i++) {
		balls[out_].push_back( std::make_shared<Ball>(state_->count_) );
		state_->count_++;
	}
	return balls;
}

double BallGenerator::TimeAdvance()
{
	return 1.0;
}

PinState::PinState(unsigned id) : id_(id), left_count_(0), right_count_(0)
{
}

std::string PinState::string() const
{
	return "#left:"+to_string(left_count_)+", #right:"+to_string(right_count_)+", #total:"+to_string(left_count_+right_count_);
}

std::string PinState::toXML() const
{
	return "<pin><left>"+to_string(left_count_)+"</left><right>"+to_string(right_count_)+"</right><total>"+to_string(left_count_+right_count_)+"</total></pin>\n";
}

Pin::Pin(unsigned id, double left_probability, std::string name) : Atomic(name), left_probability_(left_probability)
{
	setState( PinState(id) );
	in_ = AddInPort("IN");
	left_out_ = AddOutPort("LEFT OUT");
	right_out_ = AddOutPort("RIGHT OUT");

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	gen_ = std::mt19937(seed);
	dist_ = std::bernoulli_distribution(left_probability_);
}

Pin::Pin() : Pin(0)
{
//	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
//	gen_ = std::mt19937(seed);
//	dist_ = std::bernoulli_distribution(left_probability_);
}

PinState const& Pin::ExtTransition(Outbags inputs)
{
	Outbag& incoming = inputs.at(in_);
	for (auto& ball : inputs.at(in_))
		balls_.push_back(std::make_shared<Ball>(dynamic_cast<Ball const&>(*ball)));
	return *state_;
}

Outbags Pin::OutputFunction()
{
	Outbag left;
	Outbag right;
	for (auto& ball : balls_)
		if(dist_(gen_)) {
			left.push_back(ball);
			state_->left_count_++;
		} else {
			right.push_back(ball);
			state_->right_count_++;
		}

	Outbags output;
	if (left.size() > 0)
		output[left_out_] = left;
	if (right.size() > 0)
		output[right_out_] = right;

	balls_.clear();
	return output;
}

double Pin::TimeAdvance()
{
	if (balls_.size() > 0)
		return 0.1;
	return std::numeric_limits<double>::infinity();
}

BinState::BinState(unsigned id) : id_(id), count_(0)
{
}

std::string BinState::string() const
{
	return "count: " + to_string(count_);
}

std::string BinState::toXML() const
{
	return "<bin><count>"+to_string(count_)+"</count></bin>";
}

Bin::Bin(unsigned id, std::string name) : Atomic(name)
{
	setState( BinState(id) );
	in_ = AddInPort("IN");
}

Bin::Bin() : Bin(0)
{
}

BinState const& Bin::ExtTransition(Outbags inputs)
{
	state_->count_ += inputs.at(in_).size();
	return *state_;
}

GaltonBoard::GaltonBoard(unsigned height, double rate, double left_probability, std::string name) : Coupled(name), height_(height), pins_(height*(height+1)/2), bins_(height+1)
{
	generator_ = BallGenerator::create(rate);
	AddSubModel(generator_);

	for (unsigned i=0; i<pins_.size(); i++) {
		pins_[i] = Pin::create(i, left_probability, "Pin"+to_string(i));
		AddSubModel(pins_[i]);
	}

	for (unsigned i=0; i<bins_.size(); i++) {
		bins_[i] = Bin::create(i, "Bin"+to_string(i));
		AddSubModel(bins_[i]);
	}

	ConnectPorts(generator_->out_, pins_[0]->in_);

	for (unsigned i=0; i<(pins_.size() - height); i++) {
		unsigned h = std::floor( (std::sqrt(8.*i+1.)-1)/2. );
		ConnectPorts(pins_[i]->left_out_, pins_[i+h+1]->in_);
		ConnectPorts(pins_[i]->right_out_, pins_[i+h+2]->in_);
	}

	for (unsigned i=0; i<height; i++) {
		ConnectPorts(pins_[pins_.size() - height + i]->left_out_, bins_[i]->in_);
		ConnectPorts(pins_[pins_.size() - height + i]->right_out_, bins_[i+1]->in_);
	}
}

GaltonBoard::GaltonBoard(unsigned height) : GaltonBoard(height, 0)
{
}

std::vector<unsigned> GaltonBoard::getDistribution() const
{
	std::vector<unsigned> distribution(bins_.size());
	for (unsigned i=0; i<bins_.size(); i++)
		distribution[i] = bins_[i]->getBallCount();

	return distribution;
}

std::vector<double> GaltonBoard::getNormalizedDistribution() const
{
	std::vector<double> distribution(bins_.size());
	unsigned total = 0;
	for (unsigned i=0; i<bins_.size(); i++)
		total += bins_[i]->getBallCount();

	for (unsigned i=0; i<bins_.size(); i++)
		distribution[i] = (double)bins_[i]->getBallCount()/(double)total;

	return distribution;
}

std::string GaltonBoard::getDistributionAsCSV()
{
	std::stringstream ss;
	std::vector<unsigned> dist = getDistribution();

	for (unsigned i = 0; i < dist.size(); i++)
		ss << i << "," << dist[i] << std::endl;
	return ss.str();
}

std::string GaltonBoard::getNormalizedDistributionAsCSV()
{
	std::stringstream ss;
	std::vector<double> dist = getNormalizedDistribution();

	ss << std::fixed << std::setprecision(8);
	for (unsigned i = 0; i < dist.size(); i++)
		ss << i << "," << dist[i] << std::endl;
	return ss.str();
}
}
}
