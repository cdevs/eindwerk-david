/*
 * model.h
 *
 *  Created on: 7-mei-2015
 *      Author: david
 */

#ifndef EXAMPLES_GALTON_MODEL_H_
#define EXAMPLES_GALTON_MODEL_H_

#include "../../src/AtomicDevs.h"
#include "../../src/CoupledDEVS.h"
#include <memory>
#include <random>

#include <cereal/archives/binary.hpp>
#include <cereal/access.hpp>
#include <cereal/types/base_class.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/polymorphic.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/vector.hpp>

namespace cdevs_examples
{
namespace galton
{

typedef std::list<std::shared_ptr<const cdevs::EventBase> > Outbag;
typedef std::map<std::weak_ptr<cdevs::Port>, Outbag, std::owner_less<std::weak_ptr<cdevs::Port> > > Outbags;

// Forward declarations
class BallGenerator;
class Pin;
class Bin;

/**
 *
 */
class Ball : public cdevs::EventCRTP<Ball, 0>
{
public:
	Ball(unsigned id);
	std::string string() const;
	std::string toXML() const;
private:
	const unsigned id_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(id_);
		ar(cereal::base_class<cdevs::EventCRTP<Ball, 0> >(this));
	}

	template <class Archive>
	static void load_and_construct( Archive & ar, cereal::construct<Ball> & construct )
	{
		unsigned id;
		ar(id);
		construct(id);
	}
};

/**
 *
 */
class BallGeneratorState : public cdevs::State<BallGeneratorState>
{
friend class BallGenerator;
public:
	BallGeneratorState();
private:
	unsigned count_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::State<BallGeneratorState> >(this), count_);
	}
};

/**
 *
 */
class BallGenerator : public cdevs::Atomic<BallGenerator, BallGeneratorState>
{
friend class GaltonBoard;
public:
	BallGenerator(double rate);

	Outbags OutputFunction();
	double TimeAdvance();
private:
	const double rate_;
	std::weak_ptr<cdevs::Port> out_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(rate_);
		ar(cereal::base_class<cdevs::Atomic<BallGenerator, BallGeneratorState>>(this), out_);
	}

	template <class Archive>
	static void load_and_construct( Archive & ar, cereal::construct<BallGenerator> & construct )
	{
		double rate;
		ar(rate);
		construct(rate);
	}
};

/**
 *
 */
class PinState : public cdevs::State<PinState>
{
friend class Pin;
public:
	PinState(unsigned id);
	std::string string() const;
	std::string toXML() const;
private:
	const unsigned id_;
	unsigned left_count_;
	unsigned right_count_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(id_);
		ar(cereal::base_class<cdevs::State<PinState> >(this), left_count_, right_count_);
	}

	template <class Archive>
	static void load_and_construct( Archive & ar, cereal::construct<PinState> & construct )
	{
		unsigned id;
		ar(id);
		construct(id);
	}
};

/**
 *
 */
class Pin : public cdevs::Atomic<Pin, PinState>
{
friend class GaltonBoard;
public:
	Pin(unsigned id, double left_probability = 0.5, std::string name = "Pin");

	PinState const& ExtTransition(Outbags inputs);
	Outbags OutputFunction();
	double TimeAdvance();
private:
	double left_probability_;
	std::list<std::shared_ptr<Ball>> balls_;
	std::weak_ptr<cdevs::Port> in_;
	std::weak_ptr<cdevs::Port> left_out_;
	std::weak_ptr<cdevs::Port> right_out_;

	std::mt19937 gen_;
	std::bernoulli_distribution dist_;

	Pin();

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Pin, PinState>>(this), left_probability_, balls_, in_, left_out_, right_out_);
	}
};

/**
 *
 */
class BinState : public cdevs::State<BinState>
{
friend class Bin;
public:
	BinState(unsigned id);
	std::string string() const;
	std::string toXML() const;
private:
	const unsigned id_;
	unsigned count_;

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(id_);
		ar(cereal::base_class<cdevs::State<BinState> >(this), count_);
	}

	template <class Archive>
	static void load_and_construct( Archive & ar, cereal::construct<BinState> & construct )
	{
		unsigned id;
		ar(id);
		construct(id);
	}
};

/**
 *
 */
class Bin : public cdevs::Atomic<Bin, BinState>
{
friend class GaltonBoard;
public:
	Bin(unsigned id, std::string name = "Bin");
	unsigned getBallCount() const;

	BinState const& ExtTransition(Outbags inputs);
private:
	std::weak_ptr<cdevs::Port> in_;

	Bin();

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(cereal::base_class<cdevs::Atomic<Bin, BinState>>(this), in_);
	}
};

/**
 *
 */
class GaltonBoard : public cdevs::Coupled<GaltonBoard>
{
public:
	GaltonBoard(unsigned height, double rate, double left_probability = 0.5, std::string name = "GaltonBoard");
	std::vector<unsigned> getDistribution() const;
	std::vector<double> getNormalizedDistribution() const;
	std::string getDistributionAsCSV();
	std::string getNormalizedDistributionAsCSV();
private:
	const unsigned height_;

	std::shared_ptr<BallGenerator> generator_;
	std::vector<std::shared_ptr<Pin>> pins_;
	std::vector<std::shared_ptr<Bin>> bins_;

	GaltonBoard(unsigned height);

	friend class cereal::access;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar(height_);
		ar(cereal::base_class<cdevs::Coupled<GaltonBoard>>(this), generator_, pins_, bins_);
	}

	template <class Archive>
	static void load_and_construct( Archive & ar, cereal::construct<GaltonBoard> & construct )
	{
		unsigned height;
		ar(height);
		construct(height);
	}
};

inline unsigned Bin::getBallCount() const {
	return state_->count_;
}

}
}

CEREAL_REGISTER_TYPE(cdevs_examples::galton::Ball)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::BallGeneratorState)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::BallGenerator)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::PinState)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::Pin)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::BinState)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::Bin)
CEREAL_REGISTER_TYPE(cdevs_examples::galton::GaltonBoard)

#endif /* EXAMPLES_GALTON_MODEL_H_ */
