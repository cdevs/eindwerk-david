#include "TracerCustom.h"

#include <iostream>
#include <string>
#include <cfloat>

namespace cdevs {
TracerCustom::TracerCustom(std::string filename)
	: Tracer(filename)
{
}

TracerCustom::~TracerCustom()
{
}

void TracerCustom::TraceInitialize(std::shared_ptr<AtomicDevs> aDEVS, DevsTime t)
{
	std::string text = "TraceInitialize custom\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
}

void TracerCustom::TraceInternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "TraceInternal custom\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
}

void TracerCustom::TraceExternal(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "TraceExternal custom\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
}

void TracerCustom::TraceConfluent(std::shared_ptr<AtomicDevs> aDEVS)
{
	std::string text = "TraceConfluent custom\n";
	controller_->RunTrace(shared_from_this(), aDEVS, text);
}

void TracerCustom::Trace(std::shared_ptr<AtomicDevs> model, DevsTime time, std::string text)
{
	if (time > this->prevtime_) {
		text = "\n__  Current Time: " + time.string() + " " + std::string(42, '_') + " \n\n" + text;
		prevtime_ = time;
	}

	PrintString(text);
}
} /* namespace cdevs */
