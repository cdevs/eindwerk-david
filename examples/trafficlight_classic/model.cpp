/*
 * AtomicModelTest.cpp
 *
 *  Created on: 31-mrt.-2015
 *      Author: david
 */
#include "model.h"
#include <algorithm>
#include <limits>
#include <iostream>

using namespace cdevs;

namespace cdevs_examples {
namespace classic
{
TrafficLightMode::TrafficLightMode(std::string value) :
		value_(value) {
}

TrafficLight::TrafficLight(std::string name) :
		Atomic(name) {
	setState( state_red_ );
	elapsed_ = 1.5;

	INTERRUPT = AddInPort("INTERRUPT");
	OBSERVED = AddOutPort("OBSERVED");
}

TrafficLightMode const& TrafficLight::ExtTransition(
	std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<Port> > > inputs) {
	if (inputs.at(INTERRUPT).front()->getType() != kPolicemanMode)
		throw cdevs::RuntimeDevsException("External transitions on TrafficLight can only receive be PolicemanMode events.");

	const PolicemanMode& mode = dynamic_cast<PolicemanMode const&>(*inputs.at(INTERRUPT).front());
	std::string input = mode.getValue();

	std::string state = getState().getValue();

	if (input == "toManual") {
		if (state == "manual") {
			return state_manual_;
		} else if (state == "red" || state == "green" || state == "yellow") {
			return state_manual_;
		}
	} else if (input == "toAutonomous") {
		if (state == "manual") {
			return state_red_;
		} else if (state == "red" || state == "green" || state == "yellow") {
			return getState();
		}
	}
	throw RuntimeDevsException("TrafficLight has entered a non-valid state!");
}

TrafficLightMode const& TrafficLight::IntTransition() {
	std::string state = getState().getValue();

	if (state == "red")
		return state_green_;
	else if (state == "green")
		return state_yellow_;
	else if (state == "yellow")
		return state_red_;
	throw RuntimeDevsException("TrafficLight has entered a non-valid state!");
}

std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<Port> > > TrafficLight::OutputFunction() {
	std::string state = getState().getValue();

	if (state == "red")
		return { {OBSERVED, {std::make_shared<TrafficLightMode>("grey")}}};
	else if (state == "green")
		return { {OBSERVED, {std::make_shared<TrafficLightMode>("yellow")}}};
	else if (state == "yellow")
		return { {OBSERVED, {std::make_shared<TrafficLightMode>("grey")}}};
	throw RuntimeDevsException("TrafficLight has entered a non-valid state!");
}

double TrafficLight::TimeAdvance() {
	std::string state = getState().getValue();

	if (state == "red")
		return 60;
	else if (state == "green")
		return 50;
	else if (state == "yellow")
		return 10;
	else if (state == "manual")
		return std::numeric_limits<double>::infinity();
	throw RuntimeDevsException("TrafficLight has entered a non-valid state!");
}

PolicemanMode::PolicemanMode(std::string value) :
	value_(value) {
}

Policeman::Policeman(std::string name) :
		Atomic(name) {
	setState( state_idle_ );

	elapsed_ = 0.0;

	OUT = AddOutPort("OUT");
}

PolicemanMode const& Policeman::IntTransition() {
	std::string state = getState().getValue();

	if (state == "idle")
		return state_working_;
	else if (state == "working")
		return state_idle_;
	throw RuntimeDevsException("Policeman has entered a non-valid state!");
}

std::map<std::weak_ptr<Port>, std::list<std::shared_ptr<const cdevs::EventBase> >, std::owner_less<std::weak_ptr<Port> > > Policeman::OutputFunction() {
	std::string state = getState().getValue();

	if (state == "idle")
		return { {OUT, {std::make_shared<PolicemanMode>("toManual")}}};
	else if (state == "working")
		return { {OUT, {std::make_shared<PolicemanMode>("toAutonomous")}}};
	throw RuntimeDevsException("Policeman has entered a non-valid state!");
}

double Policeman::TimeAdvance() {
	std::string state = getState().getValue();

	if (state == "idle")
		return 200;
	else if (state == "working")
		return 100;
	throw RuntimeDevsException("Policeman has entered a non-valid state!");
}

TrafficSystem::TrafficSystem(std::string name) :
		Coupled(name) {
	// Declare the coupled model's output ports:
	// Autonomous, so no output ports

	// Declare the coupled model's sub-models:

	// The Policeman generating interrupts
	policeman = Policeman::create("policeman");
	AddSubModel(policeman);

	// The TrafficLight
	trafficlight = TrafficLight::create("trafficlight");
	AddSubModel(trafficlight);

	// Only connect ...
	ConnectPorts(policeman->OUT, trafficlight->INTERRUPT);
}

std::shared_ptr<BaseDevs> TrafficSystem::Select(std::list<std::shared_ptr<BaseDevs>> imm_children) const {
	if (std::find(imm_children.begin(), imm_children.end(), policeman->getPtr())
			== imm_children.end()) {
		return imm_children.front();
	} else {
		return policeman->getPtr();
	}
}

std::shared_ptr<Policeman> TrafficSystem::getPoliceMan() const
{
	return policeman;
}

std::shared_ptr<TrafficLight> TrafficSystem::getTrafficLight() const
{
	return trafficlight;
}

}
}
