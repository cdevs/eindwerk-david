       _____  ________      _______ 
      |  __ \|  ____\ \    / / ____|
   ___| |  | | |__   \ \  / / (___ 
  / __| |  | |  __|   \ \/ / \___ \ 
 | (__| |__| | |____   \  /  ____) |
  \___|_____/|______|   \/  |_____/ 
                                    
                                   

Welcome to the cDEVS project a Discrete Event Simulator adhering to the DEVS Formalism. All
information can be found on the cDevs website http://vandevondel.eu along with a user manual
and documents in correspondence with the design decisions used. 

Team:

David Dansaert
Nathan Steurs
Ian Vermeulen
Joren Van De Vondel
Lotte Vergauwen

Info: info@vandevondel.eu
