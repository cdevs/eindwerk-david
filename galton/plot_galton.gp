set datafile separator ","
set term png
set output "galtonSkew.png"
set xlabel "bin index"
set ylabel "#balls"
set title "Ball distribution of a Galton board with 20 bins, 10^6 balls and 66% left probability"
set autoscale x
plot "galtonSkew.csv" using 2 with histogram notitle

set output "galtonSkew_normalized.png"
set xlabel "bin index"
set ylabel "frequency"
set title "Normalized ball distribution of a Galton board with 20 bins, 10^6 balls and 66% left probability"
set autoscale x
plot "galtonSkew_normalized.csv" using 2 with histogram notitle
